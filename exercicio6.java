package minicursojava;

import java.util.Scanner;

public class exercicio6 {
	public static void main (String []args) {
		
		Scanner leitor = new Scanner(System.in);
		
		String frase;
		String resultado;
		
		System.out.println("Digite uma frase: ");
		frase = leitor.next();

		leitor.close();
		
		resultado = frase.replaceAll("[AEIOUaeiou]", "");
		
		System.out.println("O resultado da frase é " + resultado);
	} 
}
