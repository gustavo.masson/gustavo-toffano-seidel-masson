package minicursojava;

import java.util.Scanner;

public class exerciciopcrianca3 {
	public static double Aritimetica(double [] numeros) {
		double soma = 0;
		for (double num : numeros) {
			soma += num;
		}
		return soma/numeros.length;
	}
	public static double Ponderada (double numeros[], double pesos[]) {
		if (numeros.length != pesos.length) {
			throw new IllegalArgumentException("Arrays de números e peso tem que ser iguais.");
		}
		double somaProdutos = 0;
		double somaPesos = 0;
		
		for (int i = 0; i < numeros.length; i++) {
			somaProdutos += numeros[i] * pesos[i];
			somaPesos += pesos[i];
		}
		return somaProdutos/somaPesos;
	}
	public static void main (String []args) {
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Quantos números deseja calcular a média? ");
		int n = leitor.nextInt();
		
		double[] numeros = new double [n]; 
		
		for ( int i = 0; i< n; i++) {
			System.out.println("Digite o número" + (i+1) + ": ");
			numeros[i]=leitor.nextDouble();
			}
		double mediaAritimetica = Aritimetica(numeros);		
		System.out.println("A média Aritimetica é " + mediaAritimetica);
		
		System.out.println("Coloque os pesos da média ponderada: ");
		double [] pesos = new double[n];
		
		for (int i = 0; i < n; i++ ) {
			System.out.println("Digite o pesos:");
			pesos[i] = leitor.nextDouble();	
		}
		
		double mediaPonderada = Ponderada(numeros,pesos);
		System.out.println("A média ponderada é " + mediaPonderada);
		
		leitor.close();
	}
}