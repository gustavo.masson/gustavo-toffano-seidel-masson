package minicursojava;

import java.util.Scanner;

public class aula6atividade1 {
	public static void main (String []args) {
		
		Scanner leitor = new Scanner(System.in);
		
		int[] array  = new int[7];
		
		System.out.println("Digite um número: ");
		int numero = leitor.nextInt();
		leitor.close();

		array[0] += numero;
		array[array.length -1] +=numero;
		
		System.out.println("Array resultante: ");
		for (int i = 0; i< array.length; i++) {
			
			System.out.println(array[i]);
			
		}
	}
}
