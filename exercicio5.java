package minicursojava;

import java.util.Scanner;

public class exercicio5 {
	public static void main (String []args) {
		
		Scanner leitor = new Scanner(System.in);
		
		String frase;
		char letraAntiga;
		char letraNova;
		
		System.out.println("Digite uma frase: ");
		frase = leitor.next();
		
		System.out.println("Digite a letra a ser substituida: ");
		letraAntiga = leitor.next().charAt(0);	
		
		System.out.println("Digite a letra a substituir: ");
		letraNova = leitor.next().charAt(0);
		
		String novaFrase = frase.replace(letraAntiga, letraNova);
	
		leitor.close();

		System.out.println("A nova frase é " + novaFrase);
	
	}
}
