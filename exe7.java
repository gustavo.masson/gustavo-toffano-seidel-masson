package minicursojava;

import java.util.Scanner;

public class exe5 {
	public static void main (String args[]) {
		Scanner leitor = new Scanner(System.in);
		
		int opcao = 0;
		
		System.out.println("1- adição ");
		System.out.println("2- subtração ");
		System.out.println("3- multiplicação");
		System.out.println("4- divisão");
		System.out.println("Digite a operação que deseja: ");
		opcao = leitor.nextInt();
		
		double numero1 = 0;
		double numero2 = 0;
		double resultado = 0;
		
		switch (opcao) {
		case 1:
			System.out.println("Digite o numero 1:");
			numero1 = leitor.nextDouble();
			System.out.println("Digite o número 2: ");
			numero2 = leitor.nextDouble();
			resultado = (numero1 + numero2);
			System.out.println("O resultado da soma é " + resultado);
		break;
		case 2: 
			System.out.println("Digite o numero 1:");
			numero1 = leitor.nextDouble();
			System.out.println("Digite o número 2: ");
			numero2 = leitor.nextDouble();
			resultado = (numero1 - numero2);
			System.out.println("O resultado da subtração é " + resultado);
		break;
		case 3: 
			System.out.println("Digite o numero 1:");
			numero1 = leitor.nextDouble();
			System.out.println("Digite o número 2: ");
			numero2 = leitor.nextDouble();
			resultado = (numero1 * numero2);
			System.out.println("O resultado da multiplicação é " + resultado);
		break;
		case 4:
			System.out.println("Digite o numero 1:");
			numero1 = leitor.nextDouble();
			System.out.println("Digite o número 2: ");
			numero2 = leitor.nextDouble();
			resultado = (numero1 / numero2);
			System.out.println("O resultado da divisão é " + resultado);
		}
		leitor.close();
	}
}
