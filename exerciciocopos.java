package minicursojava;

import java.util.Scanner;

public class exerciciocopos {
	public static int CalcularCoposQuebrados(int latas, int copos){
		if (latas>copos) {
			return copos;
		}else {
			return 0;
		}
	}
	public static void main (String[]args) {
		
		//Entrada de dados 
		
		Scanner leitor = new Scanner(System.in);
		
		int bandejas = 0;
		int bandejaCopos = 0;
		int bandejaLatas = 0;
		int resultado = 0;
		int totalCoposQuebrados = 0;
		do {
		System.out.println("quantos latas tem na bandeja? ");
		bandejaLatas = leitor.nextInt();
		System.out.println("quantas copos tem na bandeja? ");
		bandejaCopos =leitor.nextInt();
		bandejas ++;
		totalCoposQuebrados += CalcularCoposQuebrados(bandejaLatas, bandejaCopos);
	
		System.out.println("O garçom levou mais bandejas? Insira S para adicionar outra bandeija." );
		}while(leitor.next().equalsIgnoreCase("S"));
		
		
		leitor.close();

		//Saída de Dados
		
		System.out.println("O garçom quebrará " + totalCoposQuebrados + " copos em " + bandejas +" bandejas");
		
	} 
}
