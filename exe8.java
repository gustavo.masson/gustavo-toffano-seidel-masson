package minicursojava;

import java.util.Scanner;

public class exe8 {
	public static void main (String args[]) {
		
		Scanner leitor = new Scanner(System.in);
	
		System.out.println("Digite o valor de X:");
		double X = leitor.nextDouble();
		System.out.println("Digite o valor de Y:");
		double Y = leitor.nextDouble();
		
		if (X>0 && Y>0) {
			System.out.println("É o primeiro quadrante");
		}else if (X<0 && Y>0) {
			System.out.println("É o segundo quadrante");
		}else if (X<0 && Y<0) {
			System.out.println("É o terceiro quadrante");
		}else if (X>0 && Y<0) {
			System.out.println("É o quarto quadrante");
		}else {
			System.out.println("Esse é a origem");
		}
		leitor.close();
	}
}
